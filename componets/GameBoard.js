import React, { useEffect, useState } from "react";
import { View, StyleSheet,Text, ImageBackground } from "react-native";

import Square from "./Square";
import { connect } from 'react-redux'

const GameBoard = (props) =>{
  let isGameOver
const [timeLeft , setTimeLeft] = useState(10)
useEffect(()=>{
if(!timeLeft)return
const timerId = setInterval(()=>{
setTimeLeft(timeLeft-1)
},1000)
return () => clearInterval(timerId)
},[timeLeft])

    return(
<ImageBackground 
        style={styles.container}
        source={require('../assets/background.png')}
        >
        <View style={styles.cont}>
      <Text style={styles.header}> Whack the Ghoul</Text>
      <Text>You have {timeLeft} second left</Text>
      <Text>{props.score} Ghouls Whacked</Text>
      </View>

     <View style={styles.game}>
     {isGameOver && <Text>GAME OVER</Text>}
<Square></Square>
<Square></Square>
<Square></Square>
<Square></Square>
<Square></Square>
<Square></Square>
<Square></Square>
<Square></Square>
<Square></Square>
<Square></Square>
<Square></Square>
<Square ></Square>
     </View>
    </ImageBackground>
    )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  game: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: 300,
    paddingTop: 10,
  },
  header: {
    fontSize:29,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 50
  },
  cont: {
  
    alignItems: 'center',
    width:300,
    height: 120
  }
});

  const mapStateToProps = state => {
    return {
      score: state.score
    }
  }

export default connect(mapStateToProps)(GameBoard)